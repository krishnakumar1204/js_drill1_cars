// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function findCarsOlderThan2000(carYears){
    if(Array.isArray(carYears)){
        let carsOlderThan2000 = [];
        for(year of carYears){
            if(year < 2000){
                carsOlderThan2000.push(year);
            }
        }
        return carsOlderThan2000;
    }
    else{
        return [];
    }
}

module.exports = findCarsOlderThan2000;