// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModelsAlphabatically(inventory){
    if(Array.isArray(inventory)){
        let carModelNames = [];
        for(car of inventory){
            carModelNames.push(car.car_model);
        }
        for(let i=0; i<carModelNames.length-1; i++){
            for(let j=0; j<carModelNames.length-i-1; j++){
                if(carModelNames[j].toLowerCase() > carModelNames[j+1].toLowerCase()){
                    let tempModelName = carModelNames[j];
                    carModelNames[j] = carModelNames[j+1];
                    carModelNames[j+1] = tempModelName;
                }
            }
        }
        return carModelNames;
    }
    else{
        return [];
    }
}

module.exports = sortCarModelsAlphabatically;