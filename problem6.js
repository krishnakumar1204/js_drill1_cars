// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function findBMWAndAudi(inventory){
    if(Array.isArray(inventory)){
        let BMWAndAudi = [];
        for(car of inventory){
            if(car.car_make=="BMW" || car.car_make=="Audi"){
                BMWAndAudi.push(car);
            }
        }
        return BMWAndAudi;
    }
    else{
        return [];
    }
}

module.exports = findBMWAndAudi;