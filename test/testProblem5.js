const inventory = require("../data.js");
const findCarsOlderThan2000 = require("../problem5.js");
const giveYear = require("../problem4.js");

try {
    let carYears = giveYear(inventory);
    let carsOlderThan2000 = findCarsOlderThan2000(carYears);
    console.log(carsOlderThan2000.length);
} catch (error) {
    console.log("Somrthing went wrong");
}