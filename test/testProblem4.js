const inventory = require("../data.js");
const giveYear = require("../problem4.js");

try {
    let carYears = giveYear(inventory);
    console.log(carYears);
} catch (error) {
    console.log("Something went wrong");
}