const inventory = require("../data.js");
const giveLastCar = require("../problem2.js");

try {
    let lastCar = giveLastCar(inventory);
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
} catch (error) {
    console.log("Something went wrong");
}